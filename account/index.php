<?PHP require_once('../assets/theme/widget/top.php'); ?>

<div class="row">
<?PHP include("theme/widget/heading.php"); ?>

<div class="col-sm-12" style="padding:40px;padding-top:20px;padding-bottom:20px;">
	
	<div class="col-sm-12" style="">
   
   <ul class="nav nav-tabs tabs-up">
      <li class="active"><a href="profile.php" data-target="#profile" class="media_node active span" data-toggle="tabajax" rel="tooltip"> Account Profile </a></li>
      <li><a href="authentication.php" data-target="#authentication" class="media_node span" data-toggle="tabajax" rel="tooltip"> Authentication</a></li>
      <li><a href="users.php" data-target="#users" class="media_node span" data-toggle="tabajax" rel="tooltip">Users</a></li>
</ul>

<div class="tab-content" style="padding-bottom:25px;padding-left:16px;padding-right:16px;">
<br/><br/>
	<div class="tab-pane active" id="profile"></div>
	<div class="tab-pane" id="authentication"></div>
	<div class="tab-pane  urlbox span8" id="users"></div>
	<div class="tab-pane active" id="settings"></div>
</div>
	
	
      
    </div>
	
</div>

</div>

<?PHP require_once('../assets/theme/widget/bottom.php'); ?>
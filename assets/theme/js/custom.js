$(document).ready(function () {
	$('.menushow').click(function () {
		$('.sidebar_nav').animate({
			'marginLeft' : "250px" //moves down
		});
	});
	
	//ON SCREEN RESIZE
	$(window).resize(function(){
	$('.sidebar_nav').removeAttr('style');
	});
	
	//AJAX TAB
	$('[data-toggle="tabajax"]').click(function(e) {
	e.preventDefault();
    var $this = $(this),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');

    $.get(loadurl, function(data) {
        $(targ).html(data);
    });

    $this.tab('show');
    return false;
	});
	
	$(".nav-tabs li.active a").click();
	
});

function postData(path, data, thisObj){
	$.ajax({
		type: "POST",
		url: path,
		data: data,
		dataType: 'JSON',
		success: function (html) {
			return "sample  data"; //html.response;
		},
		error: function (textStatus, errorThrown) {
			console.log(textStatus);
			console.log(errorThrown);
		},
		beforeSend: function () {
			//thisObj.attr("disabled","disabled");
		}
	});
}


<?PHP
define('__ROOT__', dirname(dirname(__FILE__))); 
require_once(__ROOT__.'..\..\include\config.php');
require_once(__ROOT__.'..\..\include\messages.php'); 
require_once(__ROOT__.'..\..\include\functions.php'); 
require_once(__ROOT__.'..\..\include\permission.php'); 
require_once(__ROOT__.'\..\..\\'. currentSection() .'\include\config.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../favicon.ico">

    <title><?PHP echo $page_title;?></title>
	<?PHP require_once(__ROOT__.'..\..\include\css.php');?>
	<?PHP require_once(__ROOT__.'\..\..\\'. currentSection() .'\include\css.php');?>
  </head>
<body>
    <div class="container">
		<div class="sidebar_nav">
			<div class="logo"><i class="glyphicon glyphicon-screenshot" style="top:4px;"> </i> Targetlink</div>
			<div class="list-group" style="padding-top:40px;">
			  <a href="../dashboard" class="list-group-item"><i class="glyphicon glyphicon-cog"> </i> &nbsp;Dashboard</a>
			  <a href="../websites" class="list-group-item"><i class="glyphicon glyphicon-fire"> </i> &nbsp;My Websites</a>
			  <a href="../subscribers" class="list-group-item"><i class="glyphicon glyphicon-fire"> </i> &nbsp;Subscribers</a>
			  <a href="../push" class="list-group-item"><i class="glyphicon glyphicon-link"> </i> &nbsp;PUSH Messages</a>
			  <a href="../automation" class="list-group-item"><i class="glyphicon glyphicon-link"> </i> &nbsp;Automation</a>
			</div>
			<div class="list-group" style="padding-top:20px;">
				<span class="list-group-label">YOUR ACCOUNT</span>
				<a href="../billing" class="list-group-item"><i class="glyphicon glyphicon-cog"> </i> &nbsp; My Billing</a>
				<a href="../account" class="list-group-item"><i class="glyphicon glyphicon-link"> </i> &nbsp;Account settings</a>
			</div>
		</div>

		<div class="content_area">
			<div class="col-sm-12 header one-edge-shadow"> <?PHP include('welcome-dropdown.php'); ?>
			

			</div>
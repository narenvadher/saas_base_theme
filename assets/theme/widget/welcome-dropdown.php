<div class="dropdown pull-right">
	<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" style="border:0px;">Welcome, Naren Vadher
		<span class="caret"></span>
	</button>
	<ul id="user-dropdown" class="dropdown-menu dropdown-menu-right user-dropdown-menu" role="menu" style="width:250px;">
		<li role="presentation" class="dropdown-header username-header" style="background-color:#0052CC;color:#FFF;">
			<div class="user-data" style="padding-top:4px;padding-bottom:4px;">
				<strong>Magento unit1</strong>
				<br/>
                                                                    magento.unit1@gmail.com                                                                                            
			</div>
		</li>
		<li class="account-stat dropdown-header account-stat-balance" style="background-color:#F5F5F5">
			<a href="/billing/funds/">
                                    Balance                                    
				<strong>$0</strong>
				<small class="color-info pull-right-md">Add funds</small>
			</a>
		</li>
		<li class="">
			<a href="/support/" class="">
				<span class="sp-icon icon-support"></span>
				<span>Technical support</span>
			</a>
		</li>
		<li>
			<a href="/orders/" class="clearfix">
				<div class="pull-left ">
					<span class="sp-icon icon-safe"></span> Billing Plan
				</div>
			</a>
		</li>
		<li class="">
			<a href="/settings/" class="">
				<span class="sp-icon icon-ap-settings"></span>
				<span>Account settings</span>
			</a>
		</li>
		<li class="divider"></li>
		<li>
			<a href="/logout/">
				<span class="sp-icon icon-power"></span> Log out
			</a>
		</li>
	</ul>
</div>
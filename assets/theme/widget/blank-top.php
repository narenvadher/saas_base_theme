<?PHP
define('__ROOT__', dirname(dirname(__FILE__))); 
require_once(__ROOT__.'..\..\include\config.php');
require_once(__ROOT__.'..\..\include\messages.php'); 
require_once(__ROOT__.'..\..\include\functions.php'); 
require_once(__ROOT__.'..\..\include\permission.php'); 
require_once(__ROOT__.'\..\..\\'. currentSection() .'\include\config.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../favicon.ico">

    <title><?PHP echo $page_title;?></title>
	<?PHP require_once(__ROOT__.'..\..\include\css.php');?>
	<?PHP require_once(__ROOT__.'\..\..\\'. currentSection() .'\include\css.php');?>
  </head>
<body>
<?PHP
function currentSection(){
    $path=explode("/", $_SERVER["REQUEST_URI"]);
    $section=$path[count($path)-2];
    if($section=='model'){
        return $path[count($path)-3];    
    }else if($section=='form_handle'){
        return $path[count($path)-3];    
    }else if($section=='charts'){
        return $path[count($path)-3];    
    }else{
        return $path[count($path)-2];
    }
}
function generateRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function siteURL() {
  $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || 
    $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
  $domainName = $_SERVER['HTTP_HOST'];
  if($domainName=='localhost'){
    return $protocol.$domainName ."/kavyatech/";
  }else{
    return $protocol.$domainName ."/";
  }
}

function normalizeString ($str = ''){
    $str = strip_tags($str); 
    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
    $str = strtolower($str);
    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = str_replace(' ', '-', $str);
    $str = rawurlencode($str);
    $str = str_replace('%', '-', $str);
    return $str;
}

function daysOfMonth($month,$year){
	return cal_days_in_month(CAL_GREGORIAN, $month, $year);
}

function dateFormat($mysqldate, $format){
    $phpdate = strtotime($mysqldate);
    $mysqldate = date($format, $phpdate );
    return $mysqldate;
}
?>
<?PHP require_once('../assets/theme/widget/top.php'); ?>

<div class="row">
<?PHP include("theme/widget/heading.php"); ?>

<div class="col-sm-12" style="padding:40px;padding-top:20px;padding-bottom:20px;">
	<?PHP include("theme/widget/push.php"); ?>
</div>

</div>

<?PHP require_once('../assets/theme/widget/bottom.php'); ?>
<style>
input.checkboxcheckmark									{ display:none; }
input.checkboxcheckmark + label							{ line-height:24px; cursor:pointer; }
input.checkboxcheckmark + label::before					{ content:"\f279"; vertical-align:top; margin-right:10px; font-size:24px; font-family:Material-Design-Iconic-Font; color:#cacdcf; transition: all ease .3s; }
input.checkboxcheckmark + label:hover::before			{ color:#1e88e5; }
input.checkboxcheckmark:checked + label::before			{ content:"\f26a"; color:#1e88e5; }
input.checkboxcheckmark:disabled + label::before		{ color: #eaedef; }
input.checkboxcheckmark:disabled + label:hover::before	{ color: #eaedef; cursor: default; }
input.checkboxcheckmark:disabled + label				{ color: #aaadaf; }
input.checkboxcheckmark:disabled + label > a			{ color: #2691d9; }
label.blank-checkbox{margin:0px;}
.table>tbody>tr>th, .table>thead>tr>th{border-top:0px;border-bottom:0px;color:#9DA2A6;font-weight:400;10px 10px;}

</style>
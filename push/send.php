<?PHP require_once('../assets/theme/widget/top.php'); ?>

<div class="row">
<?PHP include("theme/widget/heading-send.php"); ?>

<div class="col-sm-12" style="padding:40px;padding-top:20px;padding-bottom:20px;">
	<div class="col-lg-6 col-md-5">
	<?PHP include("theme/widget/send-form.php");?>
	</div>
	
	<div class="col-lg-6 col-md-5 hidden-sm hidden-xs" style="margin-top:25px;">
    <?PHP include("theme/widget/send-form-preview.php");?>
    </div>
</div>
</div>

<?PHP require_once('../assets/theme/widget/bottom.php'); ?>
<style>
input.checkboxcheckmark									{ display:none; }
input.checkboxcheckmark + label							{ line-height:24px; cursor:pointer; }
input.checkboxcheckmark + label::before					{ content:"\f279"; vertical-align:top; margin-right:10px; font-size:24px; font-family:Material-Design-Iconic-Font; color:#cacdcf; transition: all ease .3s; }
input.checkboxcheckmark + label:hover::before			{ color:#1e88e5; }
input.checkboxcheckmark:checked + label::before			{ content:"\f26a"; color:#1e88e5; }
input.checkboxcheckmark:disabled + label::before		{ color: #eaedef; }
input.checkboxcheckmark:disabled + label:hover::before	{ color: #eaedef; cursor: default; }
input.checkboxcheckmark:disabled + label				{ color: #aaadaf; }
input.checkboxcheckmark:disabled + label > a			{ color: #2691d9; }
label.blank-checkbox{margin:0px;}
.table>tbody>tr>th, .table>thead>tr>th{border-top:0px;border-bottom:0px;color:#9DA2A6;font-weight:400;10px 10px;}

.panel {border:0px;}
label{font-weight:500}
.panel-body{padding:0px;}
.col-lg-6{padding-left:0px;}


input[type=text], input[type=file], input[type=password], input[type=email], select{
    border: 1px solid #DCDEE0;
    vertical-align: middle;
    border-radius: 3px;
    height: 40px;
    padding: 0px 16px;
    font-size: 14px;
    color: #555555;
    background-color: #ffffff;	
}
.form-control select{height: 80px!important;}
.form-control{
	box-shadow:none;
    border: 1px solid #DCDEE0;
    vertical-align: middle;
    border-radius: 3px;
    font-size: 14px;
    color: #555555;
    background-color: #ffffff;	
	}
.form-control:focus {
        border-color: #0052CC;
        box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.075) inset, 0px 0px 0px rgba(255, 100, 255, 0.5);
}
	


</style>
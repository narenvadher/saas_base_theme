
<div style="border-width:0px; background:linear-gradient(45deg, #135FAC 1%, #1e88e5 64%, #40BAF5 97%);border-radius:3px;padding:20px;">
			<center><h3 style="color:#ffffff;margin:0px;">
				Message Preview
			</h3></center>
			<span style="display:block; position:relative;margin-top:10px;">
				<center><input type="text" name="campaign_url" value="Google Chrome" readonly="readonly" style="color:#ffffff; border-width:0px; background-color:rgba(0,0,0, 0.2);"></center>
				<br/>
				
				<div class="preview-push preview-push-chrome-win">
                            <span class="close-notify">×</span>
                            <div class="pr-icon-side">
                                <div class="pr-icon-wrapper">
                                    <img id="preview-push-chrome-win" src="https://login.sendpulse.com/files/push/6711714/websites/0b44806b0801729f637a8c27b9223f9a/icons/2883dacf80001.png" class="pr-icon">
                                </div>
                            </div>
                            <div class="pr-message-wrapper">
                                <div class="pr-notify-title"></div>
                                <div class="pr-notify-message">
                                    Your message content                                </div>
                                <div class="pr-notify-site">quiteeasy.sendpulse.com</div>
                                <img class="cog-icon" width="14" height="14" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDM2OS43OTMgMzY5Ljc5MiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzY5Ljc5MyAzNjkuNzkyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPGc+CgkJPGc+CgkJCTxwYXRoIGQ9Ik0zMjAuODMsMTQwLjQzNGwtMS43NTktMC42MjdsLTYuODctMTYuMzk5bDAuNzQ1LTEuNjg1YzIwLjgxMi00Ny4yMDEsMTkuMzc3LTQ4LjYwOSwxNS45MjUtNTIuMDMxTDMwMS4xMSw0Mi42MSAgICAgYy0xLjEzNS0xLjEyNi0zLjEyOC0xLjkxOC00Ljg0Ni0xLjkxOGMtMS41NjIsMC02LjI5MywwLTQ3LjI5NCwxOC41N0wyNDcuMzI2LDYwbC0xNi45MTYtNi44MTJsLTAuNjc5LTEuNjg0ICAgICBDMjEwLjQ1LDMuNzYyLDIwOC40NzUsMy43NjIsMjAzLjY3NywzLjc2MmgtMzkuMjA1Yy00Ljc4LDAtNi45NTcsMC0yNC44MzYsNDcuODI1bC0wLjY3MywxLjc0MWwtMTYuODI4LDYuODZsLTEuNjA5LTAuNjY5ICAgICBDOTIuNzc0LDQ3LjgxOSw3Ni41Nyw0MS44ODYsNzIuMzQ2LDQxLjg4NmMtMS43MTQsMC0zLjcxNCwwLjc2OS00Ljg1NCwxLjg5MmwtMjcuNzg3LDI3LjE2ICAgICBjLTMuNTI1LDMuNDc3LTQuOTg3LDQuOTMzLDE2LjkxNSw1MS4xNDlsMC44MDUsMS43MTRsLTYuODgxLDE2LjM4MWwtMS42ODQsMC42NTFDMCwxNTkuNzE1LDAsMTYxLjU1NiwwLDE2Ni40NzR2MzguNDE4ICAgICBjMCw0LjkzMSwwLDYuOTc5LDQ4Ljk1NywyNC41MjRsMS43NSwwLjYxOGw2Ljg4MiwxNi4zMzNsLTAuNzM5LDEuNjY5Yy0yMC44MTIsNDcuMjIzLTE5LjQ5Miw0OC41MDEtMTUuOTQ5LDUyLjAyNUw2OC42MiwzMjcuMTggICAgIGMxLjE2MiwxLjExNywzLjE3MywxLjkxNSw0Ljg4OCwxLjkxNWMxLjU1MiwwLDYuMjcyLDAsNDcuMy0xOC41NjFsMS42NDMtMC43NjlsMTYuOTI3LDYuODQ2bDAuNjU4LDEuNjkzICAgICBjMTkuMjkzLDQ3LjcyNiwyMS4yNzUsNDcuNzI2LDI2LjA3Niw0Ny43MjZoMzkuMjE3YzQuOTI0LDAsNi45NjYsMCwyNC44NTktNDcuODU3bDAuNjY3LTEuNzQybDE2Ljg1NS02LjgxNGwxLjYwNCwwLjY1NCAgICAgYzI3LjcyOSwxMS43MzMsNDMuOTI1LDE3LjY1NCw0OC4xMjIsMTcuNjU0YzEuNjk5LDAsMy43MTctMC43NDUsNC44NzYtMS44OTNsMjcuODMyLTI3LjIxOSAgICAgYzMuNTAxLTMuNDk1LDQuOTYtNC45MjQtMTYuOTgxLTUxLjA5NmwtMC44MTYtMS43MzRsNi44NjktMTYuMzFsMS42NC0wLjY0M2M0OC45MzgtMTguOTgxLDQ4LjkzOC0yMC44MzEsNDguOTM4LTI1Ljc1NXYtMzguMzk1ICAgICBDMzY5Ljc5MywxNTkuOTUsMzY5Ljc5MywxNTcuOTE0LDMyMC44MywxNDAuNDM0eiBNMTg0Ljg5NiwyNDcuMjAzYy0zNS4wMzgsMC02My41NDItMjcuOTU5LTYzLjU0Mi02Mi4zICAgICBjMC0zNC4zNDIsMjguNTA1LTYyLjI2NCw2My41NDItNjIuMjY0YzM1LjAyMywwLDYzLjUyMiwyNy45MjgsNjMuNTIyLDYyLjI2NEMyNDguNDE5LDIxOS4yMzgsMjE5LjkyLDI0Ny4yMDMsMTg0Ljg5NiwyNDcuMjAzeiIgZmlsbD0iIzRkNGQ0ZCIvPgoJCTwvZz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K">
                            </div>

                                                                                                <div class="pr-notify-big-image" id="chrome-big-image">
                                        <!--<img src="/img/push/prompt-help-text-ru.png" />-->
                                    </div>
                                    <div class="pr-notify-btn-1"></div>
                                    <div class="pr-notify-btn-2"></div>
                                                            
                        </div>
						
						
			</span>
		</div>
		

<style>
.pr-icon-wrapper,
.pr-notify-big-image {
    text-align: center
}
.preview-push {
    margin-left: auto;
    margin-right: auto;
    width: 360px;
    overflow: hidden;
    position: relative;
    background: #fff;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .5);
}
.pr-icon-side {
    display: table-cell;
    vertical-align: top;
    width: 82px
}
.pr-icon-wrapper {
    display: table-cell;
    vertical-align: middle;
    background-color: #fff;
    width: 82px;
    height: 82px
}
.pr-message-wrapper {
    display: table-cell;
    vertical-align: top;
    padding: 10px 15px;
    position: relative;
    width: 278px
}
.pr-notify-title {
    font-size: 13.2px;
    margin-bottom: 5px;
    max-height: 38px
}
.pr-notify-message,
.pr-notify-site {
    font-size: 12px
}
.pr-notify-message,
.pr-notify-title {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    box-orient: vertical
}
.pr-notify-message {
    margin-bottom: 5px;
    -webkit-line-clamp: 3;
    line-clamp: 3;
    max-height: 52px
}
.pr-notify-site {
    color: #7F7F7F
}
.pr-notify-btn-1,
.pr-notify-btn-2 {
    padding: 10px 15px;
    font-size: 12px;
    border-top: 1px solid #EAEAEA;
    -webkit-line-clamp: 1;
    line-clamp: 1
}
.pr-notify-btn-1:empty,
.pr-notify-btn-2:empty {
    display: none
}
.pr-notify-btn-1 img,
.pr-notify-btn-2 img {
    display: inline-block;
    width: 15px;
    height: 15px;
    margin-right: 12px;
    position: relative;
    top: -2px
}
.pr-notify-big-image img[src=""],
.pr-notify-big-image:empty,
.pr-notify-btn-1 img[src=""],
.pr-notify-btn-2 img[src=""],
.preview-push-safari-mac .close-notify,
.preview-push-safari-mac .cog-icon,
.preview-push-safari-mac .pr-notify-site {
    display: none
}
.close-notify,
.cog-icon {
    position: absolute;
    right: 10px
}
.pr-notify-big-image img {
    max-width: 310px;
    max-height: 220px;
    margin: 10px auto
}
.preview-push .pr-icon {
    max-width: 82px;
    max-height: 82px
}
.close-notify {
    top: 5px;
    font-weight: 700;
    font-size: 17px;
    color: #777
}
.cog-icon {
    bottom: 10px
}
.preview-push-chrome-win .cog-icon {
    opacity: .6
}
.preview-push-firefox {
    border: 1px solid #D1D1D1;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .05)
}
.preview-push-firefox .pr-icon-side {
    padding-top: 20px;
    vertical-align: middle;
    width: 94px
}
.preview-push-firefox .pr-icon-wrapper {
    background-color: transparent
}
.preview-push-firefox .pr-icon {
    max-width: 80px;
    max-height: 80px;
    margin: 7px
}
.preview-push-firefox .pr-message-wrapper {
    padding: 5px 25px 10px 0
}
.preview-push-firefox .pr-notify-title {
    font-size: 12px;
    font-weight: 700;
    margin-left: -87px;
    -webkit-line-clamp: 1;
    line-clamp: 1;
    max-height: 18px
}
.preview-push-firefox .pr-notify-message {
    -webkit-line-clamp: 5;
    line-clamp: 5;
    min-height: 59px;
    max-height: 88px
}
.preview-push-firefox .pr-notify-message,
.preview-push-firefox .pr-notify-title {
    margin-bottom: 5px;
    color: #191919
}
.preview-push-firefox .pr-notify-site {
    font-size: 10px;
    color: #6D6D6D
}
.preview-push-firefox .close-notify {
    font-weight: 400;
    font-size: 20px;
    top: -1px;
    right: 8px
}
.preview-push-chrome-mac,
.preview-push-safari-mac {
    font-family: -apple-system, 'Helvetica Neue', Helvetica, Arial, sans-serif;
    -webkit-font-smoothing: subpixel-antialiased
}
.preview-push-chrome-mac {
    box-shadow: 0 10px 15px rgba(0, 0, 0, .15);
    border: 1px solid #ACACAC
}
.preview-push-chrome-mac .pr-icon {
    max-width: 80px;
    max-height: 80px
}
.preview-push-chrome-mac .pr-notify-title {
    font-size: 14px
}
.preview-push-chrome-mac .pr-notify-message,
.preview-push-chrome-mac .pr-notify-title {
    margin-bottom: 3px
}
.preview-push-chrome-mac .pr-notify-message,
.preview-push-chrome-mac .pr-notify-site {
    font-size: 13px
}
.preview-push-safari-mac {
    width: 325px;
    height: 60px;
    box-shadow: 0 2px 10px rgba(0, 0, 0, .2);
    background-color: rgba(255, 255, 255, .8);
    -webkit-backdrop-filter: blur(20px);
    border-radius: 6px
}
.preview-push-safari-mac .pr-icon-side {
    width: 42px;
    vertical-align: middle
}
.preview-push-safari-mac .pr-icon-wrapper {
    height: 32px;
    background: 0 0;
    width: 42px;
    text-align: right
}
.preview-push-safari-mac .pr-icon {
    max-width: 32px;
    max-height: 32px
}
.preview-push-safari-mac .pr-message-wrapper {
    padding: 9px 10px;
    height: 60px;
    display: table-cell;
    vertical-align: middle
}
.preview-push-safari-mac .pr-notify-message,
.preview-push-safari-mac .pr-notify-title {
    color: #4C4C4C;
    line-height: 1.2
}
.preview-push-safari-mac .pr-notify-title {
    font-size: 14px;
    font-weight: 700;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-bottom: 2px;
    -webkit-line-clamp: 1;
    line-clamp: 1;
    width: 260px;
    max-height: 15px
}
.preview-push-safari-mac .pr-notify-message {
    margin-bottom: 0;
    font-size: 11px;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    max-height: 27px
}
</style>
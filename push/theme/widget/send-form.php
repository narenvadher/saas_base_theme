  <div id="form-add-task" class="form">
            <div class="form-group">
                <label class="control-label">
                    List of Recipients                </label>
                
                    <select id="website" onchange="addTask.changeWebsite();" class="form-control">
                                                    <option value="2865" data-protocol="http" data-url="quiteeasy.in" data-subdomain="quiteeasy" data-active-subscriptions="2" data-icon="2883dacf80001.png" data-unique-sitename="0b44806b0801729f637a8c27b9223f9a" selected="">
                                http://quiteeasy.in (2 subscribers)
                            </option>
                                            </select>
                                <div class="bs-callout bs-callout-red bs-callout-sm hidden" id="error-wrapper"></div>
                                <p id="segment-checkbox-block" class="help-text checkbox hidden">
                    <label data-placement="right" title="" data-original-title="There are no subscribers that match the current filter for this website.">
                        <input onchange="addTask.toggleSegmentOptions();" id="segment-box" type="checkbox">
                        Segment the list                        <span class="count-segment text-muted">
                            (Recipients: <strong id="cnt-subscriptions">2</strong>)
                        </span>
                    </label>
                </p>
                                <div class="segment-options hidden" id="segment-options"></div>

            </div>
            <div class="form-group " id="taskTitle">
                <label class="control-label">
                    Title                </label>
                <div class="has-feedback js-variables-allowed split-var">
                    <input maxlength="100" id="notify-title" data-target=".pr-notify-title" type="text" class="form-control valid js-target-input notify-title" required="required" placeholder="up to 50 characters">
                    <div class="js-variables-block form-control-feedback dropdown-feedback hidden"></div>
                    <div class="up_to_50_title_description_block help-block small hidden">
                        <span class="text-warning">Recommended message length of 50 characters exceeded</span>
                        <em class="pull-right light-grey-text countPushMessageTitle">0</em>
                    </div>
                </div>                
            </div>
            <div class="form-group" id="taskMessage">
                <label class="control-label">
                    Message Text</label>
                <div class="has-feedback js-variables-allowed split-var">
                    <textarea maxlength="512" id="notify-text" data-target=".pr-notify-message" class="form-control valid js-target-input notify-text" rows="2" style="resize: vertical;" required="required" placeholder="up to 125 characters"></textarea>
                    <div class="js-variables-block form-control-feedback dropdown-feedback hidden"></div>
                    <div class="up_to_125_text_description_block help-block small hidden">
                        <span class="text-warning">Recommended message length of 125 characters exceeded</span>
                        <em class="countPushMessageText pull-right light-grey-text"></em>
                    </div>
                </div>
                
            </div>
            <div class="form-group">
                <label class="control-label">
                    PUSH Message Link</label>
                <input id="notify-link" data-target=".pr-notify-site" type="text" class="form-control valid" required="required" placeholder="https://example.com">
            </div>
            <div class="form-group" id="changePicture">
               
                <div class="change-icon" id="taskPicture">
                   
                    <input id="task-icon" type="hidden" value="" autocomplete="off">
                    <input id="task-icon-1" type="hidden" value="" autocomplete="off">
                    <input id="task-icon-2" type="hidden" value="" autocomplete="off">
                    <div class="media form-option js-upload-icon-block split-var">
                        <div class="media-left pushicon-uploader icon-uploader">
                            <div class="hover-effect"><span class="sp-icon icon-browser-upload animate"></span></div>
                            <div id="pushicon_container_image" class="icon-thumb pushicon_thumb animate picture-element-principal" style="border: 1px solid rgb(221, 221, 221); width: 96px; height: 96px; cursor: pointer; overflow: hidden; position: relative; background-image: url(&quot;/files/push/6711714/websites/0b44806b0801729f637a8c27b9223f9a/icons/2883dacf80001.png&quot;); background-size: cover;"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="picture-element-image" style="position: relative; cursor: pointer; height: 96px;"><input type="hidden" name="push_icon_image" id="push_icon_image" class="picture-element-image-directory"><form method="POST" enctype="multipart/form-data" action="/push/tasks/picture-cut-upoload"> <input name="file-push_icon_image" type="file" accept="image/*" title="" style="display: block; position: absolute; left: 0px; top: 0px; width: 94px; height: 94px; font-size: 100pt; opacity: 0; z-index: 10; overflow: hidden;"></form></div>
                        </div>
                        <div class="media-body">
							 <label class="control-label">
                        Replace standard image:
                    </label><br/>
                            <button class="btn btn-default btn-sm js-btn-upload-icon">Change the image</button>
                            <p class="help-block small">
                                Recommended size: 128*128px <br> JPG, PNG, GIF up to 200KB                            </p>
                        </div>
                    </div>

                </div>
            </div>

            
            <link href="/css/my/bootstrap-select.css?v=1.10" rel="stylesheet" type="text/css">
            <script src="/js/my/bootstrap-select.js?v=1.10" type="text/javascript"></script>

            <script>
                $(document).ready(function () {
                    $('.js-icon-select').selectpicker({
                        size: 7,
                        width: '100%'
                    });
                    $('.js-icon-select').change(function () {
                        var image = $('#iconSelect option:selected').val();
                    })
                });
            </script>

            <div class="control-group">
                <p>
                    <a href="#additionalPushOptions" class="dotted-toggle collapsed" data-toggle="collapse" aria-expanded="false">
                        <span>Additional options</span>
                        <span class="caret"></span>
                    </a>
                </p>
                <div class="collapse additional-push-options" id="additionalPushOptions" aria-expanded="false">
                    <div class="bs-callout bs-callout-sm bs-callout-info">
                        These features may not work in all browsers (Chrome/Chromium only)                    </div>
                    <div class="form-group">
                        <label>Add a large image</label>
                        <input id="task-image" type="hidden" value="" autocomplete="off">
                        <div class="media form-option js-upload-icon-block split-var">
                            <div class="media-left pushicon-uploader icon-uploader">
                                <div class="hover-effect"><span class="sp-icon icon-browser-upload animate"></span></div>
                                <div id="pushicon_container_big_image" class="icon-thumb pushicon_thumb animate picture-element-principal" style="border: 1px solid rgb(221, 221, 221); width: 96px; height: 96px; cursor: pointer; overflow: hidden; position: relative;"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="picture-element-image" style="position: relative; cursor: pointer; height: 96px;"><input type="hidden" name="push_icon_image" id="push_icon_image" class="picture-element-image-directory"><form method="POST" enctype="multipart/form-data" action="/push/tasks/picture-cut-upoload"> <input name="file-push_icon_image" type="file" accept="image/*" title="" style="display: block; position: absolute; left: 0px; top: 0px; width: 94px; height: 94px; font-size: 100pt; opacity: 0; z-index: 10; overflow: hidden;"></form></div>
                            </div>
                            <div class="media-body">
                                <button class="btn btn-default btn-sm js-btn-upload-icon">Choose the image</button>
                                <p class="help-block small">
                                    Recommended size: 300*200 px<br>JPG, PNG, GIF up to 200 KB                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Add buttons for web push</label>
                        <div class="action-button-block js-action-button-block">
                            <div class="checkbox">
                                <span class="switcher">
                                    <input type="checkbox" class="switcher-toggle" id="actionButton1Toggle">
                                    <label for="actionButton1Toggle"></label>
                                </span>
                                <label for="actionButton1Toggle">First button</label>
                            </div>
                            <div class="row control-group">
                                <div class="col-sm-6">
                                    <label>Button text</label>
                                    <input type="text" class="form-control js-target-input" id="push_button_1_text" value="First button" placeholder="" data-target=".pr-notify-btn-1" disabled="">
                                </div>

                                <div class="col-sm-6">
                                    <label>Button icon</label>

                                    <!--<input type="url" id="actionButton1Image" data-target=".pr-notify-btn-1" class="form-control" placeholder="(не обязательно)" disabled>-->

                                    <div class="label-selector">
                                        <div class="btn-group bootstrap-select disabled js-icon-select show-menu-arrow" style="width: 100%;"><button type="button" class="btn dropdown-toggle disabled btn-default" data-toggle="dropdown" data-id="actionButton1Image" tabindex="-1" title="No"><span class="filter-option pull-left">No</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">No</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f30d.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f31f.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f338.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f355.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f379.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f380.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="7"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f381.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="8"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f3b5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="9"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f3c6.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="10"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="11"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44d.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="12"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44e.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="13"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44f.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="14"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f453.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="15"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f464.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="16"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f465.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="17"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f480.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="18"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f48c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="19"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f48e.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="20"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4a4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="21"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4a5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="22"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4a9.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="23"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4aa.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="24"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4ac.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="25"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4b3.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="26"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4bc.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="27"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4c1.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="28"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4c2.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="29"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4c4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="30"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4cb.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="31"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4d4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="32"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4e2.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="33"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4e3.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="34"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4e5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="35"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f504.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="36"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f511.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="37"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f512.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="38"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f514.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="39"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f519.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="40"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f525.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="41"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f553.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="42"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f600.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="43"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f602.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="44"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f60e.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="45"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f61c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="46"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f635.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="47"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f680.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="48"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f6a6.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="49"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f6ab.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="50"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2139.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="51"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/231a.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="52"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/231b.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="53"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2600.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="54"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2615.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="55"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2665.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="56"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26a1.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="57"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26c4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="58"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26c5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="59"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26d4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="60"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26f5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="61"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2705.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="62"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2708.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="63"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/270c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="64"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/270f.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="65"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2728.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="66"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2753.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="67"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2b50.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select id="actionButton1Image" data-target=".pr-notify-btn-1" autocomplete="off" class="js-icon-select selectpicker show-menu-arrow" disabled="" tabindex="-98">
                                            <option selected="selected" value="">No</option>
                                                <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f30d.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f30d.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f31f.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f31f.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f338.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f338.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f355.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f355.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f379.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f379.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f380.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f380.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f381.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f381.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f3b5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f3b5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f3c6.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f3c6.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44d.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44d.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44e.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44e.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44f.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44f.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f453.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f453.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f464.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f464.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f465.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f465.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f480.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f480.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f48c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f48c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f48e.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f48e.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4a4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4a4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4a5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4a5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4a9.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4a9.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4aa.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4aa.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4ac.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4ac.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4b3.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4b3.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4bc.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4bc.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4c1.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4c1.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4c2.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4c2.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4c4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4c4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4cb.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4cb.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4d4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4d4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4e2.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4e2.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4e3.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4e3.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4e5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4e5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f504.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f504.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f511.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f511.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f512.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f512.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f514.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f514.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f519.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f519.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f525.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f525.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f553.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f553.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f600.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f600.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f602.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f602.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f60e.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f60e.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f61c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f61c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f635.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f635.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f680.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f680.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f6a6.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f6a6.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f6ab.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f6ab.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2139.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2139.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/231a.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/231a.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/231b.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/231b.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2600.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2600.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2615.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2615.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2665.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2665.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26a1.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26a1.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26c4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26c4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26c5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26c5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26d4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26d4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26f5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26f5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2705.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2705.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2708.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2708.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/270c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/270c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/270f.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/270f.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2728.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2728.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2753.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2753.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2b50.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2b50.png <!-- cdn url -->
    </option>
                                        </select></div>
                                    </div>

                                </div>

                            </div>
                            <div>
                                <label>Button link</label>
                                <input id="push_button_1_link" type="url" class="form-control" disabled="">
                            </div>

                        </div>

                        <div class="action-button-block js-action-button-block">
                            <div class="checkbox">
                                <span class="switcher">
                                    <input type="checkbox" class="switcher-toggle" id="actionButton2Toggle">
                                    <label for="actionButton2Toggle"></label>
                                </span>
                                <label for="actionButton2Toggle">Second button</label>
                            </div>
                            <div class="row control-group">
                                <div class="col-sm-6">
                                    <label>Button text</label>
                                    <input type="text" class="form-control js-target-input" id="push_button_2_text" value="Second button" placeholder="" data-target=".pr-notify-btn-2" disabled="">
                                </div>

                                <div class="col-sm-6">
                                    <label>Button icon</label>
                                    <!--<input type="url" id="actionButton2Image" data-target=".pr-notify-btn-2" class="form-control" placeholder="(не обязательно)" disabled>-->

                                    <div class="label-selector">
                                        <div class="btn-group bootstrap-select disabled js-icon-select show-menu-arrow" style="width: 100%;"><button type="button" class="btn dropdown-toggle disabled btn-default" data-toggle="dropdown" data-id="actionButton2Image" tabindex="-1" title="No"><span class="filter-option pull-left">No</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">No</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f30d.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f31f.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f338.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f355.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f379.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f380.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="7"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f381.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="8"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f3b5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="9"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f3c6.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="10"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="11"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44d.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="12"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44e.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="13"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f44f.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="14"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f453.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="15"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f464.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="16"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f465.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="17"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f480.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="18"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f48c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="19"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f48e.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="20"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4a4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="21"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4a5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="22"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4a9.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="23"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4aa.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="24"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4ac.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="25"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4b3.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="26"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4bc.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="27"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4c1.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="28"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4c2.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="29"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4c4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="30"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4cb.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="31"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4d4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="32"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4e2.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="33"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4e3.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="34"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f4e5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="35"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f504.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="36"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f511.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="37"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f512.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="38"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f514.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="39"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f519.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="40"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f525.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="41"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f553.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="42"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f600.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="43"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f602.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="44"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f60e.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="45"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f61c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="46"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f635.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="47"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f680.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="48"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f6a6.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="49"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/1f6ab.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="50"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2139.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="51"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/231a.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="52"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/231b.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="53"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2600.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="54"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2615.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="55"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2665.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="56"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26a1.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="57"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26c4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="58"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26c5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="59"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26d4.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="60"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/26f5.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="61"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2705.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="62"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2708.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="63"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/270c.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="64"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/270f.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="65"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2728.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="66"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2753.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="67"><a tabindex="0" class="" style="" data-tokens="null">
            <div align="center">
            <span><img src="/img/my/emoji/2b50.png" alt="" border="0" vspace="2"></span>
            </div>
            <span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select id="actionButton2Image" data-target=".pr-notify-btn-2" autocomplete="off" class="js-icon-select selectpicker show-menu-arrow" disabled="" tabindex="-98">
                                            <option selected="selected" value="">No</option>
                                                <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f30d.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f30d.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f31f.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f31f.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f338.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f338.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f355.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f355.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f379.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f379.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f380.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f380.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f381.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f381.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f3b5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f3b5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f3c6.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f3c6.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44d.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44d.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44e.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44e.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f44f.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f44f.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f453.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f453.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f464.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f464.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f465.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f465.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f480.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f480.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f48c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f48c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f48e.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f48e.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4a4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4a4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4a5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4a5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4a9.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4a9.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4aa.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4aa.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4ac.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4ac.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4b3.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4b3.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4bc.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4bc.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4c1.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4c1.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4c2.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4c2.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4c4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4c4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4cb.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4cb.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4d4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4d4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4e2.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4e2.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4e3.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4e3.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f4e5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f4e5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f504.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f504.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f511.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f511.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f512.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f512.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f514.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f514.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f519.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f519.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f525.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f525.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f553.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f553.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f600.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f600.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f602.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f602.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f60e.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f60e.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f61c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f61c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f635.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f635.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f680.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f680.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f6a6.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f6a6.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/1f6ab.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/1f6ab.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2139.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2139.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/231a.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/231a.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/231b.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/231b.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2600.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2600.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2615.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2615.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2665.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2665.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26a1.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26a1.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26c4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26c4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26c5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26c5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26d4.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26d4.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/26f5.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/26f5.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2705.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2705.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2708.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2708.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/270c.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/270c.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/270f.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/270f.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2728.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2728.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2753.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2753.png <!-- cdn url -->
    </option>
    <option data-content="
            <div align=&quot;center&quot;>
            <span><img src=&quot;/img/my/emoji/2b50.png&quot; alt=&quot;&quot; border=&quot;0&quot; vspace=&quot;2&quot;></span>
            </div>
            ">
        /img/my/emoji/2b50.png <!-- cdn url -->
    </option>
                                        </select></div>
                                    </div>
                                </div>

                            </div>
                            <div>
                                <label>Button link</label>
                                <input id="push_button_2_link" type="url" class="form-control" disabled="">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
                        <br>
            <div class="btn-group send-btn">
				<input type="button" value="Send PUSH Message" onclick="confirmMassRestart()">
            </div>

            <div class="btn-group dropup adv-settings">
                <button type="button" id="toggleAdvSettings" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-cog"></span>
                </button>
                <div class="dropdown-menu schedule-menu adv-options-menu">
                    <div class="js-adv-menu">
                        <label class="not_bold small dashed hover-help" title="Set the time frame for web push delivery. After this period web push messages will not be delivered">
                            Push lifetime                        </label>
                        <div class="form-group ">
                            <select id="ttlSettings" class="form-control input-sm">
                                <option value="900">15 minutes</option>
                                <option value="1800">30 minutes</option>
                                <option value="3600">1 hour</option>
                                <option value="21600">6 hours</option>
                                <option value="86400" selected="">24 hours</option>
                                                            </select>
                        </div>
                    </div>
                    <div id="send-throttling-block" class="js-adv-menu">
                        <label class="not_bold small dashed hover-help" title="If you want to send a campaign to a large list, we recommend sending it in parts to reduce load on your site.">
                            Send throttling                        </label>
                        <div class="form-group ">
                            <select id="stretch_time" class="form-control input-sm">
                                <option value="0" selected="">send immediately</option>
                                <option value="3600">During 1 hours</option>
                                <option value="7200">During 2 hours</option>
                                <option value="10800">During 3 hours</option>
                                <option value="14400">During 4 hours</option>
                                <option value="18000">During 5 hours</option>
                            </select>
                        </div>
                    </div>
                                    <div class="utm-options js-adv-menu">
                    <div class="divider"></div>
                                        <div class="checkbox">
                        <label class="" for="external_stat">
                            <input type="checkbox" onchange="addTask.checkExternalStat(this);" id="external_stat">
                            UTM Codes                        </label>
                    </div>
                    <!-- EXTERNAL STAT START -->
                        <!-- UTM_CAMPAIGN -->
                        <div class="form-group">
                            <label class="small not_bold">utm_campaign</label>
                            <div class="has-feedback js-variables-allowed split-var">
                                <input placeholder="" value="{{campaign_id}}" type="text" id="external_stat_campaign_name" disabled="" class="form-control input-sm">
                                <span class="form-control-feedback glyphicon glyphicon-info-sign hover-effect" data-toggle="tooltip" data-original-title="Use {{campaign_id}} variable to add the id of your push campaign."></span>
                            </div>
                        </div>
                                            </div>
                    <!-- EXTERNAL STAT END -->
                </div>
            </div>
            <a class="btn btn-link btn-lg" id="task_send_test_push_link"><small class="dotted small_xs">Send a test push</small> </a>

            <div class="hidden" id="variables_block">
                <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle" type="button" aria-expanded="false">
                    <span class="sp-icon icon-curlybrace-2"></span>
                    <span class="caret"></span>
                </button>
                <ul role="menu" class="js-variables-list dropdown-menu dropdown-menu-right condition_value_dropdown"></ul>
            </div>


        </div>
		
<div class="hidden">
    <div class="collapse change-icon" id="taskPictureBlank" aria-expanded="false">        
        <div class="media form-option js-upload-icon-block split-var">
            <div class="media-left pushicon-uploader icon-uploader">
                <div class="hover-effect"><span class="sp-icon icon-browser-upload animate"></span></div>
                <div class="icon-thumb pushicon_thumb animate"> </div>
            </div>
            <div class="media-body">
                <button class="btn btn-default btn-sm js-btn-upload-icon">Choose the image</button>
                <p class="help-block small">
                    Recommended size: 128*128px <br> JPG, PNG, GIF up to 200KB                </p>
            </div>
        </div>

    </div>

</div>
<?PHP require_once('../assets/theme/widget/top.php'); ?>

<div class="row" style="padding:40px;">


<h2>Dashboard</h2>
<div class="fast-links" style="text-align:right">
<input type="button" value="Send PUSH" onclick="confirmMassRestart()"> &nbsp;&nbsp;
<input type="button" value="Add New Website" onclick="confirmMassRestart()">
</div>
<br/>
<br/>
<div class="col-sm-12">
<div class="row dtable index-stat">
                        <div class="col-sm-3 col-xs-6" style="padding-left:0px;">
                            <div class="well text-center">
                                <h2 class="color-active">4</h2>
                                <span>Total Subscriber</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="well text-center">
                                <h2 class="color-primary">0</h2>
                                <span>Active Campaign</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="well text-center">
                                <h2 class="color-info">0</h2>
                                <span>Active Automation</span>
                            </div>
                        </div>
						<div class="col-sm-3 col-xs-6" style="padding-right:0px;">
                            <div class="well text-center">
                                <h2 class="color-info">0</h2>
                                <span>Active Automation</span>
                            </div>
                        </div>
                    </div>

</div>

<div class="row dtable index-stat">
                        <div class="col-sm-12 col-xs-24">
                            <div class="well text-center">
                                <span>Total Subscriber chart</span>
                            </div>
                        </div>
</div>

<div class="col-sm-12">My Websites/Shops</div>

<div class="col-sm-12">
<div class="tabs-responsive">
		<div class="tabs-container">
			<ul class="nav nav-tabs">
				<li class="active"><a href="/">Instances</a></li>
									<li><a href="/snapshots/">Snapshots</a></li>
													<li><a href="/iso/">ISO</a></li>
													<li><a href="/startup/">Startup Scripts</a></li>
													<li><a href="/sshkeys/">SSH Keys</a></li>
													<li><a href="/dns/">DNS</a></li>
							</ul>
			<button class="button-tab btn btn-prev" type="button" data-click="prev-tab"><span class="fonticon fonticon_chevron_left"></span></button>
			<button class="button-tab btn btn-next" type="button" data-click="next-tab"><span class="fonticon fonticon_chevron_right"></span></button>
		</div>
	</div>
<div class="card w-75">
  <div class="card-block">
    <h3 class="card-title">Card title</h3>
    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
    <a href="#" class="btn btn-primary">Button</a>
  </div>
</div>


<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" width="100" src="http://icons.iconseeker.com/png/fullsize/nature/pink-flower.png" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Middle aligned media</h4>
    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
  </div>
</div>


<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" width="100" src="http://icons.iconseeker.com/png/fullsize/nature/pink-flower.png" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Middle aligned media</h4>
    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
  </div>
</div>

<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" width="100" src="https://i2.wp.com/www.pi-supply.com/wp-content/uploads/2015/12/motioneye-256x256.png?resize=256%2C256&ssl=1" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">www.kavyatech.in</h4>
    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
  </div>
</div>
</div>

<hr/>
<div class="btn-group" role="group" aria-label="...">
  <button type="button" class="btn btn-default">Left</button>
  <button type="button" class="btn btn-default">Middle</button>
  <button type="button" class="btn btn-default">Right</button>
</div>
<hr/>
<ul class="nav nav-pills">
  <li role="presentation" class="active"><a href="#">Home</a></li>
  <li role="presentation"><a href="#">Profile</a></li>
  <li role="presentation"><a href="#">Messages</a></li>
</ul>

<hr/>
<div class="page-header">
  <h3>Example page header <small>Subtext for header</small></h3>
</div>

<hr/>
<div class="alert alert-success" role="alert">
  <a href="#" class="alert-link">...</a>
</div>
<div class="alert alert-info" role="alert">
  <a href="#" class="alert-link">...</a>
</div>
<div class="alert alert-warning" role="alert">
  <a href="#" class="alert-link">...</a>
</div>
<div class="alert alert-danger" role="alert">
  <a href="#" class="alert-link">...</a>
</div>

<hr/>
<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    60%
  </div>
</div>

<hr/>

<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" width="100" src="http://icons.iconseeker.com/png/fullsize/nature/pink-flower.png" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Middle aligned media</h4>
    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
  </div>
</div>

<hr/>
<div class="list-group"> <a href="#" class="list-group-item active"> <h4 class="list-group-item-heading">List group item heading</h4> <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p> </a> <a href="#" class="list-group-item"> <h4 class="list-group-item-heading">List group item heading</h4> <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p> </a> <a href="#" class="list-group-item"> <h4 class="list-group-item-heading">List group item heading</h4> <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p> </a> </div>
<hr/>

<div class="panel panel-default">
  <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Panel content
  </div>
</div>

<hr/>

<div class="panel panel-default"> <div class="panel-heading">Panel heading</div> <div class="panel-body"> <p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> </div> <table class="table"> <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>Mark</td> <td>Otto</td> <td>@mdo</td> </tr> <tr> <th scope="row">2</th> <td>Jacob</td> <td>Thornton</td> <td>@fat</td> </tr> <tr> <th scope="row">3</th> <td>Larry</td> <td>the Bird</td> <td>@twitter</td> </tr> </tbody> </table> </div>



</div>
<style>
.dropdown-menu:before {
  position: absolute;
  top: -7px;
  left: 200px;
  display: inline-block;
  border-right: 7px solid transparent;
  border-bottom: 7px solid #0052CC;
  border-left: 7px solid transparent;
  border-bottom-color: rgba(0, 0, 0, 0.2);
  content: '';
}

.dropdown-menu:after {
  position: absolute;
  top: -6px;
  left: 200px;
  display: inline-block;
  border-right: 6px solid transparent;
  border-bottom: 6px solid #0052CC;
  border-left: 6px solid transparent;
  content: '';
}
.dropdown-menu{
	border:1px solid #e3e3e3;
	border-top-left-radius:0px;
	border-top-right-radius:0px;
	padding-top:0px;
	
}
.btn-default.active, .btn-default:active, .open>.dropdown-toggle.btn-default{background-color:#FFF;color:#0747A6}
.btn-default.active, .btn-default:active, .open>.dropdown-toggle.btn-default{background-color:#FFF;color:#0747A6}
.btn-default.hover{background-color:red}

input[type=submit], input[type=button], .button, a.button{
    display: inline-block;
    vertical-align: middle;
    padding: 12px 20px;
    margin: 0px;
    font-size: 15px;
    font-weight: bold;
    line-height: 20px;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    color: #ffffff;
    background-color: #0052CC;
    border-radius: 3px;
    border: none;
    -webkit-appearance: none;
    transition: all ease-in .1s;
	}

</style>
<?PHP require_once('../assets/theme/widget/bottom.php'); ?>
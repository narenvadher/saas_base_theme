<?php
//PUT THIS HEADER ON TOP OF EACH UNIQUE PAGE
session_start();
if (isset($_SESSION['username'])) {
    header("location:index.php");
}
?>
<?PHP require_once('../assets/theme/widget/blank-top.php'); ?>

<br/><br/><center><img width="300" src="https://www.digitalocean.com/assets/media/logos-badges/png/DO_Logo_Horizontal_Blue-3db19536.png" ></center>
<div id="page-wrap">
	<div class="col-md-12">
		<div class="col-md-6" style="padding:0px;"><h4>Sign in</h4> </div>
		<div class="col-md-6" style="height:50px;padding-top:12px;text-align:right"> <span>or <a href="#" class="login-register-switch-link">create an account</a></span></div>
	
		
		<div class="social-buttons">
			<a href="include/google/index.php" class="btn btn-google"><i class="fa fa-google"></i> Login with Google</a>
			<a href="include/facebook/index.php" class="btn btn-facebook"><i class="fa fa-facebook"></i> Login with Facebook</a>
		</div>
		<br/>
		<center><h4>OR</h4></center>
		<form class="form-signin" name="form1" method="post" action="checklogin.php">
				<div id="message"></div>
				<div class="form-group">
					 <label class="sr-only" for="exampleInputEmail2">Email address</label>
					  <input name="email" type="text" class="form-control" placeholder="Email address" required="" autofocus>
        		</div>
				<div class="form-group">
					 <label class="sr-only" for="exampleInputPassword2">Password</label>
					 <input name="password" type="password" class="form-control" placeholder="Password" required="">
				</div>
				
				<div class="col-md-6" style="padding:0px;">
				<div class="checkbox">
					 <label>
					 <input type="checkbox"> Remember me
					 </label>
				</div>
				</div>
				<div class="col-md-6 pull-right" style="padding-right:0px;"><button name="Submit" type="submit" class="btn btn-primary btn-block submit">Sign in</button>
				</div>
				<div style="clear:both;"></div>
				<div class="help-block"><a href="">Forget the password ?</a></div>
		 </form>
	</div>
</div>
<style>
* { margin: 0; padding: 0; }
body{background:none}
html { 
	background: url(https://login.sendpulse.com/img/bg-overlayered.jpg) no-repeat center center fixed; 
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}

#page-wrap {width:460px; height:480px;;background-color:red;margin: 50px auto; padding: 20px; background: white; background-color:#FFF;padding:40px;border-radius:4px;border:1px solid #c1cbd4}

.login-box{}
</style>					 
<?PHP require_once('../assets/theme/widget/blank-bottom.php'); ?>


$(".social-buttons a").click(function(evt){
	evt.preventDefault();
	var a=$(this).attr('href');
	socialLogin(a, 580, 470);
});

var newWindow = null;
var socialLogin = function(url, w, h) {
    // Fixes dual-screen position
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 3) - (h / 3)) + dualScreenTop;
	if(newWindow!=null){
		if(!newWindow.closed) {
			newWindow.focus();
			return;
		}
	}
    newWindow = window.open( url, '_blank', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left );

    // Puts focus on the newWindow
    if( window.focus ) {
        newWindow.focus();
    }
};
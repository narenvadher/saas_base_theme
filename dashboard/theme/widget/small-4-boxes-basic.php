<div class="row dtable index-stat">
	<div class="col-sm-3 col-xs-6">
		<div class="well text-center" style="box-shadow: 2px 2px 0px #DEEBFF;border:1px solid #d8d8d8;">
			<h2 class="color-active">4</h2>
			<span>My Websites</span>
		</div>
	</div>
	<div class="col-sm-3 col-xs-6">
		<div class="well text-center" style="box-shadow: 2px 2px 0px #DEEBFF;border:1px solid #d8d8d8;">
			<h2 class="color-info">457000</h2>
			<span>Total Subscriber</span>
		</div>
	</div>
	<div class="col-sm-3 col-xs-6">
		<div class="well text-center" style="box-shadow: 2px 2px 0px #DEEBFF;border:1px solid #d8d8d8;">
			<h2 class="color-primary">8</h2>
			<span>Active PUSH</span>
		</div>
	</div>
	<div class="col-sm-3 col-xs-6">
		<div class="well text-center" style="box-shadow: 2px 2px 0px #DEEBFF;border:1px solid #d8d8d8;">
			<h2 class="color-info">12</h2>
			<span>Active Automation</span>
		</div>
	</div>
</div>

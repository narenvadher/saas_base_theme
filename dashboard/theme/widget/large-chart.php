<div class="well text-center" style="padding:0px;">
  <div class="row" >
    <div class="col-md-5" style="text-align:left;padding:20px;padding-left:35px;">
      <h4 class="stat-all-graph-title">
        Statistics of recent PUSH                                     
      </h4>
    </div>
    <div class="col-md-7" style="padding:20px;padding-right:35px;">
      <div class="form-inline pull-right-md period-filter">
        <div class="form-group pull-right">
          <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-sm btn-default active">
              <input onchange="stat_tasks.getInfoTaskByGraph();" type="radio" name="options" id="days" autocomplete="off" checked=""> By days                                                
            </label>
            <label class="btn btn-sm btn-default">
              <input onchange="stat_tasks.getInfoTaskByGraph();" type="radio" name="options" id="weeks" autocomplete="off"> By weeks                                                
            </label>
          </div>
          <div class="form-group">
            <select onchange="stat_tasks.changePeriodSelect();" id="period" class="form-control input-sm">
              <option value="week" selected="">During a week
              </option>
              <option value="month">During a month
              </option>
              <option value="quarter">During a quater
              </option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row" > <h1>Coming soon</h1>
  </div>
  <div class="row" style="background-color:#f5f5f5;margin:0px;height:80px;border-top:1px solid #d8d8d8;border-bottom-left-radius:4px;border-bottom-right-radius:4px;" >
	<div class="row" style="padding-top:20px;">
                                <div class="col-sm-2 col-xs-3 col-sm-offset-2">
                                    <h2 id="cnt_tasks" class="color-primary">2</h2>
                                    <span>campaigns</span>
                                </div>
                                <div class="col-sm-2 col-xs-3">
                                    <h2 id="cnt_send" class="color-blue">4</h2>
                                    <span>sent</span>
                                </div>
                                <div class="col-sm-2 col-xs-3">
                                    <h2 id="cnt_deliv" class="color-info">2</h2>
                                    <span><strong id="deliv_prc">50%</strong> delivered</span>
                                </div>
                                <div class="col-sm-2 col-xs-3">
                                    <h2 id="cnt_redirect" class="color-success">0</h2>
                                    <span><strong id="redirect_prc">0%</strong> clicks</span>
                                </div>
                            </div>
  
  </div>
</div>

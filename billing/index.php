<?PHP require_once('../assets/theme/widget/top.php'); ?>

<div class="row">
<?PHP include("theme/widget/heading.php"); ?>

<div class="col-sm-12" style="padding:40px;padding-top:20px;padding-bottom:20px;">
	
	<div class="col-sm-12" style="">
   
   <ul class="nav nav-tabs tabs-up">
      <li class="active"><a href="plan" data-target="#plan" class="media_node active span" data-toggle="tabajax" rel="tooltip"> My Plan </a></li>
      <li><a href="payment-options" data-target="#payment" class="media_node span" data-toggle="tabajax" rel="tooltip"> Payment Options</a></li>
      <li><a href="history" data-target="#history" class="media_node span" data-toggle="tabajax" rel="tooltip">History</a></li>
</ul>

<div class="tab-content" style="padding-bottom:25px;padding-left:16px;padding-right:16px;">
<br/><br/>
	<div class="tab-pane active" id="plan"></div>
	<div class="tab-pane" id="payment"></div>
	<div class="tab-pane  urlbox span8" id="history"></div>
</div>
	
	
      
    </div>
	
</div>

</div>

<?PHP require_once('../assets/theme/widget/bottom.php'); ?>
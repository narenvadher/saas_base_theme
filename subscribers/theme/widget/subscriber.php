<table class="table subscriptions">
    <thead>
        <tr>
			<th width="40">
				<input type="checkbox" class="checkboxcheckmark" id="massactiontoggle" onclick="checkUncheckAll(this);">
				<label class="blank-checkbox" for="massactiontoggle"></label>
			</th>
            <th>ID, Browser</th>
			<th>OS</th>
            <th>Region</th>
            <th>Subscription date</th>
			<th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
		<tr class="first-row">
			<td><input type="checkbox" class="checkboxcheckmark" id="massactiontoggle1"><label class="blank-checkbox" for="massactiontoggle1"></label></td>
            <td><a href="">Chrome 59 </a><br/>9aa5-cf4d-d61f-a93a-a762</td>
			<td><span class="fonticon_ubuntu" style="vertical-align:middle;"></span></td>
            <td><span><img src="https://my.vultr.com/_images/flags/flagsm_de.png" style="border-radius:3px;"></span> India, Ahmedabad</td>
            <td>July 14, 2017 - 17:42</td>
            <td>Unsubscribed</td>
			<td>-</td>
        </tr>
		<tr>
			<td><input type="checkbox" class="checkboxcheckmark" id="massactiontoggle2"><label class="blank-checkbox" for="massactiontoggle2"></label></td>
            <td><a href="" style="font-weight:500">Chrome 59 </a><br/>9aa5-cf4d-d61f-a93a-a762</td>
            <td><span class="fonticon_iso" style="vertical-align:middle;"></span></td>
			<td>  India, Ahmedabad </td>
            <td>July 14, 2017 - 17:42</td>
			<td><span class="status_success">Subscribed</span></td>
            <td>-</td>
        </tr>
		<tr>
			<td style=""><input type="checkbox" class="checkboxcheckmark" id="massactiontoggle2"><label class="blank-checkbox" for="massactiontoggle2"></label></td>
            <td colspan="6"><button id="disableSubcriptionButton" class="btn btn-xs panel-btn gray-service-btn disabled" disabled="disabled" onclick="$('#subscriberDialogOffAll').modal('show');" title="Disable subscribers"><span class="sp-icon icon-user-4-remove"></span> Disable subscribers</button></td>
        </tr>
	</tbody>
<table>

<span class="icon-alt icon-1"></span>


<style>
.fonticon_cm_de{
	background-image: url(https://my.vultr.com/_images/flags/flagsm_de.png)
    height: 18px;
    width: 27px;
    border-radius: 3px;
    vertical-align: middle;
}
.status_success{
	color: #7cb342;
    white-space: nowrap;
}
table.subscriptions td{vertical-align:middle!important}
table.subscriptions td a{font-weight:500}
.fonticon_ubuntu::before{
	content: "\e90c";
    font-family: "icons";
	font-size:24px;
	color:#1e88e5;
	
}
.fonticon_windows::before{
	content: "\e90d";
    font-family: "icons";
	font-size:24px;
	color:#1e88e5;
}

.fonticon_iso::before{
	content: "\e929";
    font-family: "icons";
	font-size:24px;
	color:#1e88e5;
	
}

.icon-alt { font-family: 'pe-icon-set-weather'; margin: 5px; }


</style>
<?PHP require_once('../assets/theme/widget/top.php'); ?>

<div class="row">
<?PHP include("theme/widget/heading-add.php"); ?>

<div class="col-sm-12" style="padding:40px;padding-top:20px;padding-bottom:20px;">
	<div class="form alert alert-info">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button id="protocol-select-btn" type="button" value="https" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sp-icon icon-locked  https"></span>
                            <span class="sp-icon icon-unlocked http hidden"></span>
                            <span id="js-protocol-text">HTTPS</span>://
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" id="protocol-select-menu">
                            <li class="active">
                                <a href="#" data-protocol="https">
                                    <span class="sp-icon icon-locked color-light"></span>
                                    HTTPS://
                                </a>
                            </li>
                            <li class="">
                                <a href="#" data-protocol="http">
                                    <span class="sp-icon icon-unlocked color-light"></span>
                                    HTTP://
                                </a>
                            </li>
                        </ul>
                    </div>
                    <input onkeydown="$(this).removeClass('inp-error');" type="url" id="site_url" class="form-control" aria-label="..." placeholder="yourdomain.com" value="">
                </div>
                <div class="help-block small collapse" id="http-descr-text" aria-expanded="false" style="height: 0px;">
                    Push notifications do not work properly for http(non-secure) sites. Push notifications will be sent from your subdomain on SendPulse website. The subscription request will be displayed in a special integrated panel.                </div>
                <div id="site-available" class="js-result-text inline-block hidden">
                    <div class="bold small color-success">
                        <span class="glyphicon glyphicon-ok-sign"></span>
                        The website is available on <span id="result-text-protocol">HTTPS</span>
                    </div>
                </div>
            </div>
			
</div>

</div>

<?PHP require_once('../assets/theme/widget/bottom.php'); ?>
<?PHP require_once('../assets/theme/widget/top.php'); ?>
<div class="row" style="padding:40px;">

<div class="col-sm-6">
<div class="dropdown pull-left">
	<button type="button" id="menu1" data-toggle="dropdown" aria-expanded="false" style="cursor:pointer;background-color:#FFF;border:0px;"><font style="font-size:22px;padding-right:4px;">www.kavyatech.in</font>
		<span class="caret"></span>
	</button>
	<ul id="user-dropdown" class="dropdown-menu dropdown-menu-left user-dropdown-menu" role="menu" style="width:250px;">
			<li class="">
				<a href="#" class="">
					<span>www.kavyatech.in</span>
				</a>
			</li>
			<li class="">
				<a href="#" class="">
					<span>push.kavyatech.in</span>
				</a>
			</li>
			<li class="">
				<a href="#" class="">
					<span>www.quiteeasy.in</span>
				</a>
			</li>
			<li class="">
				<a href="#" class="">
					<span>www.sample.com</span>
				</a>
			</li>
	</ul>
	</div>
</div>
<div class="col-sm-6">
<div class="fast-links" style="text-align:right">
<input type="button" value="Send PUSH" onclick="confirmMassRestart()"> &nbsp;&nbsp;
<input type="button" value="Add New Website" onclick="confirmMassRestart()">
</div>
</div>
    
   <div class="col-sm-12" style="margin-top:40px;">
   
   <ul class="nav nav-tabs tabs-up">
      <li class="active"><a href="statistic.php" data-target="#statistic" class="media_node active span" data-toggle="tabajax" rel="tooltip"> Statistic </a></li>
      <li><a href="subscribers.php" data-target="#subscribers" class="media_node span" data-toggle="tabajax" rel="tooltip"> Subscribers</a></li>
      <li><a href="push.php" data-target="#push" class="media_node span" data-toggle="tabajax" rel="tooltip">Recent PUSH</a></li>
	  <li><a href="settings.php" data-target="#settings" class="media_node span" data-toggle="tabajax" rel="tooltip">Settings</a></li>
</ul>

<div class="tab-content" style="padding-bottom:25px;padding-left:16px;padding-right:16px;">
<br/><br/>
	<div class="tab-pane active" id="statistic"></div>
	<div class="tab-pane" id="subscribers"></div>
	<div class="tab-pane  urlbox span8" id="push"></div>
	<div class="tab-pane active" id="settings"></div>
</div>
	
	
      
    </div>
</div>

<style>
/*DROPDOWN MENU*/
.dropdown-menu:before {
  position: absolute;
  top: -7px;
  left: 200px;
  display: inline-block;
  border-right: 7px solid transparent;
  border-bottom: 7px solid #FFF;
  border-left: 7px solid transparent;
  border-bottom-color: rgba(0, 0, 0, 0.2);
  content: '';
}

.dropdown-menu:after {
  position: absolute;
  top: -6px;
  left: 200px;
  display: inline-block;
  border-right: 6px solid transparent;
  border-bottom: 6px solid #FFF;
  border-left: 6px solid transparent;
  content: '';
}
.dropdown-menu{
	border:1px solid #e3e3e3!important;
	border-radius:4px!important;
	
}
.btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open>.dropdown-toggle.btn-default {
    color: #0747A6;
    background-color: #FFF;
    border-color: #FFF; /*set the color you want here*/
}
.btn-default:active, 
.btn-default.active, 
.open .dropdown-toggle.btn-default { 
 background-color: #FFF; 
} 

.nav-tabs > li > a:hover {background-color:#FFF;border:0px;border-bottom:2px solid #CCC}


.nav-tabs>li>a{font-size:15px;
    position: relative;
    padding: 14px 2px;
    margin: 0 14px;
    color: #616366!important;
    line-height: 20px;
    -webkit-transition: color ease-in .15s;
    transition: color ease-in .15s;
	font-weight:300;
}
.table>tbody>tr>th, .table>thead>tr>th{border-top:0px;border-bottom:0px;color:#9DA2A6;font-weight:400;10px 10px;}


input.checkboxcheckmark									{ display:none; }
input.checkboxcheckmark + label							{ line-height:24px; cursor:pointer; }
input.checkboxcheckmark + label::before					{ content:"\f279"; vertical-align:top; margin-right:10px; font-size:24px; font-family:Material-Design-Iconic-Font; color:#cacdcf; transition: all ease .3s; }
input.checkboxcheckmark + label:hover::before			{ color:#1e88e5; }
input.checkboxcheckmark:checked + label::before			{ content:"\f26a"; color:#1e88e5; }
input.checkboxcheckmark:disabled + label::before		{ color: #eaedef; }
input.checkboxcheckmark:disabled + label:hover::before	{ color: #eaedef; cursor: default; }
input.checkboxcheckmark:disabled + label				{ color: #aaadaf; }
input.checkboxcheckmark:disabled + label > a			{ color: #2691d9; }

label.blank-checkbox{margin:0px;}
</style>
<?PHP require_once('../assets/theme/widget/bottom.php'); ?>